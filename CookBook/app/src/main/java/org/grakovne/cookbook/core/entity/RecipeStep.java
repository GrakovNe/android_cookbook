package org.grakovne.cookbook.core.entity;

import com.orm.SugarRecord;

import java.io.Serializable;

public class RecipeStep extends SugarRecord implements Serializable {
    private Long id;
    private Long recipeID;
    private String instruction;
    private int stepNumber;

    public RecipeStep() {

    }

    public RecipeStep(Long recipeID, int stepNumber, String instruction) {
        setRecipeID(recipeID);
        setStepNumber(stepNumber);
        setInstruction(instruction);
    }

    public int getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber) {
        this.stepNumber = stepNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Illegal ID");
        }
        this.id = id;
    }

    public Long getRecipeID() {
        return recipeID;
    }

    public void setRecipeID(Long recipeID) {
        if (recipeID == null) {
            throw new IllegalArgumentException("Illegal recipe ID");
        }
        this.recipeID = recipeID;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        if (instruction == null || instruction.isEmpty()) {
            throw new IllegalArgumentException("Illegal instruction");
        }
        this.instruction = instruction;
    }

    @Override
    public String toString() {
        return "RecipeStep{" +
                "id=" + id +
                ", recipeID=" + recipeID +
                ", instruction='" + instruction + '\'' +
                ", stepNumber=" + stepNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecipeStep that = (RecipeStep) o;

        if (stepNumber != that.stepNumber) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (recipeID != null ? !recipeID.equals(that.recipeID) : that.recipeID != null)
            return false;
        return !(instruction != null ? !instruction.equals(that.instruction) : that.instruction != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (recipeID != null ? recipeID.hashCode() : 0);
        result = 31 * result + (instruction != null ? instruction.hashCode() : 0);
        result = 31 * result + stepNumber;
        return result;
    }
}
