package org.grakovne.cookbook.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.grakovne.cookbook.R;
import org.grakovne.cookbook.core.entity.Dish;
import org.grakovne.cookbook.core.entity.Ingredient;
import org.grakovne.cookbook.core.entity.Recipe;
import org.grakovne.cookbook.core.services.DishService;
import org.grakovne.cookbook.core.services.RecipeService;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class DishDetails extends AppCompatActivity {

    public static final String SELECTED_DISH = "SelectedDish";
    public static final String SELECTED_RECIPE = "SelectedRecipe";
    public static final String EDIT_DISH = "Edit Dish";
    public static final String DISH = "Dish";
    public static final String ADD_INGREDIENT = "Add ingredient";
    public static final String EDIT_INGREDIENT = "Edit ingredient";
    public static final String INGREDIENT = "Ingredient";

    @InjectView(R.id.dish_name_text)
    TextView dishNameText;
    @InjectView(R.id.dish_ingredients_list)
    ListView dishIngredientsList;
    @InjectView(R.id.cook_button)
    Button cookButton;
    @InjectView(R.id.add_ingredient_button)
    Button addIngredientButton;

    Dish dish = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dish_details);
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Книга рецептов");

        final Intent intent = getIntent();

        if (intent.hasExtra(SELECTED_DISH)) {
            dish = (Dish) intent.getSerializableExtra(SELECTED_DISH);

            dishNameText.setText(dish.getDishName());
            ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, convertIngredientsToDish(dish));
            dishIngredientsList.setAdapter(adapter);
        }

        dishIngredientsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Ingredient ingredient = dish.getIngredients().get(position);
                Intent editIngredientIntent = new Intent(getApplicationContext(), AddIngredientActivity.class);
                editIngredientIntent.putExtra(EDIT_INGREDIENT, true);
                editIngredientIntent.putExtra(DISH, dish);
                editIngredientIntent.putExtra(INGREDIENT, ingredient);
                startActivity(editIngredientIntent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dish_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.change_dish) {
            Intent intent = new Intent(this, AddDishActivity.class);
            intent.putExtra(EDIT_DISH, true);
            intent.putExtra(DISH, dish);
            startActivity(intent);

            return true;
        }

        if (id == R.id.delete_dish) {
            DishService.deleteDish(dish.getId());
            Intent intent = new Intent(this, DishList.class);
            startActivity(intent);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private List<String> convertIngredientsToDish(Dish dish) {
        List<String> ingredients = new ArrayList<>();

        for (Ingredient ingredient : dish.getIngredients()) {
            ingredients.add(ingredient.getIngredientName());
        }

        return ingredients;
    }

    @OnClick(R.id.cook_button)
    public void onClick() {
        Recipe recipe = RecipeService.getRecipeByDishId(dish.getId());

        Intent intent = new Intent(this, RecipeStepDetails.class);
        intent.putExtra(SELECTED_RECIPE, recipe);
        startActivity(intent);
    }

    @OnClick (R.id.add_ingredient_button)
    public void onAddIngredientButton(){
        Intent intent = new Intent(this, AddIngredientActivity.class);
        intent.putExtra(ADD_INGREDIENT, true);
        intent.putExtra(DISH, dish);

        startActivity(intent);
    }

}
