package org.grakovne.cookbook.core.entity;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Dish extends SugarRecord implements Serializable{
    @Ignore
    private List<Ingredient> ingredients;

    private Long id;

    private String dishName;

    public Dish(String dishName) {
        setDishName(dishName);
        ingredients = new ArrayList<Ingredient>();
    }

    public Dish() {

    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        if (ingredients == null) {
            throw new IllegalArgumentException("Illegal ingredients");
        }
        this.ingredients = ingredients;
    }

    public Long getId() {
        return id;
    }

    public void addIngredient(Ingredient ingredient) {
        ingredients.add(ingredient);
    }

    public void deleteIngredient(Long ingredientID) {
        for (int i = 0; i < ingredients.size(); i++) {
            if (ingredients.get(i).getId() == ingredientID) {
                ingredients.remove(i);
                break;
            }
        }
    }

    public void setId(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Illegal Identity");
        }
        this.id = id;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        if (dishName == null || dishName.isEmpty()) {
            throw new IllegalArgumentException("Illegal dish name");
        }
        this.dishName = dishName;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "ingredients=" + ingredients +
                ", id=" + id +
                ", dishName='" + dishName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dish dish = (Dish) o;

        if (ingredients != null ? !ingredients.equals(dish.ingredients) : dish.ingredients != null)
            return false;
        if (id != null ? !id.equals(dish.id) : dish.id != null) return false;
        return !(dishName != null ? !dishName.equals(dish.dishName) : dish.dishName != null);

    }

    @Override
    public int hashCode() {
        int result = ingredients != null ? ingredients.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (dishName != null ? dishName.hashCode() : 0);
        return result;
    }
}
