package org.grakovne.cookbook.core.dao;

import org.grakovne.cookbook.core.entity.Ingredient;

import java.util.List;

public class IngredientDAO {

    public static Long addIngredient(Ingredient ingredient) {
        return ingredient.save();
    }

    public static void deleteIngredient(Long id) {
        Ingredient ingredient = Ingredient.findById(Ingredient.class, id);

        if (ingredient == null) {
            throw new RuntimeException("Cannot delete non-existent ingredient");
        }

        ingredient.delete();
    }

    public static List<Ingredient> getIngredient(String ingredientName) {
        List<Ingredient> ingredientList = Ingredient.find(Ingredient.class, "ingredient_name = ?", ingredientName);
        return ingredientList;
    }

    public static Ingredient getIngredient(Long id) {
        Ingredient ingredient = Ingredient.findById(Ingredient.class, id);
        return ingredient;
    }
}
