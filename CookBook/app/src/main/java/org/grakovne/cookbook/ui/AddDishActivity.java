package org.grakovne.cookbook.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.grakovne.cookbook.R;
import org.grakovne.cookbook.core.entity.Dish;
import org.grakovne.cookbook.core.services.DishService;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class AddDishActivity extends AppCompatActivity {
    public static final String EDIT_DISH = "Edit Dish";
    public static final String NEW_DISH = "New Dish";
    Dish dish;


    @InjectView(R.id.add_dish_hint)
    TextView addDishHint;

    @InjectView(R.id.add_dish_name)
    EditText addDishName;

    @InjectView(R.id.add_dish_button)
    Button addDishButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_dish);
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Книга рецептов");

        Intent intent = getIntent();

        if (intent.hasExtra(NEW_DISH)) {
            addDishHint.setText(R.string.add_dish_hint);
            dish = null;
        }

        if (intent.hasExtra(EDIT_DISH)) {
            addDishHint.setText(R.string.change_dish_hint);
            dish = (Dish) intent.getSerializableExtra("Dish");
            addDishName.setText(dish.getDishName());
        }

    }


    @OnClick(R.id.add_dish_button)
    public void onAddDishButtonClick() {
        String newDishName = addDishName.getText().toString();

        if (!checkDishName(newDishName)) {
            Toast.makeText(getApplicationContext(), R.string.empty_dish_name, Toast.LENGTH_SHORT).show();
            return;
        }

        if (dish == null) {
            DishService.createNewDish(newDishName);
        } else {
            DishService.changeDishName(dish.getId(), newDishName);
        }

        Intent intent = new Intent(this, DishList.class);
        startActivity(intent);

    }


    private boolean checkDishName(String dishName) {
        if (dishName == null || dishName.isEmpty()) {
            return false;
        }

        return true;
    }

}
