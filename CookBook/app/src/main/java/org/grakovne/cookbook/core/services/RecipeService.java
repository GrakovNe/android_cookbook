package org.grakovne.cookbook.core.services;

import org.grakovne.cookbook.core.dao.RecipeDAO;
import org.grakovne.cookbook.core.entity.Dish;
import org.grakovne.cookbook.core.entity.Recipe;

import java.util.NoSuchElementException;

public class RecipeService {
    public static Long createRecipe(Long dishId){
        Recipe recipe = new Recipe(dishId);
        RecipeDAO.addRecipe(recipe);

        return recipe.getId();
    }

    public static Recipe getRecipeByDishId(Long dishId){
        Dish dish = DishService.getDishByID(dishId);

        Recipe recipe = RecipeDAO.getRecipeByDishID(dishId);
        checkFound(recipe);

        return recipe;
    }

    public static Recipe getRecipeById(Long id){
        Recipe recipe = RecipeDAO.getRecipe(id);
        checkFound(recipe);

        return recipe;
    }

    public static void deleteRecipeById(Long id){
        Recipe recipe = getRecipeById(id);
        RecipeDAO.deleteRecipe(recipe.getId());
    }

    public static void deleteRecipeByDishId(Long dishId){
        Dish dish = DishService.getDishByID(dishId);
        Recipe recipe = RecipeDAO.getRecipeByDishID(dishId);
        deleteRecipeById(recipe.getId());
    }

    private static void checkFound(Recipe recipe){
        if (recipe == null){
            throw new NoSuchElementException("Recipe not found");
        }
    }
}
