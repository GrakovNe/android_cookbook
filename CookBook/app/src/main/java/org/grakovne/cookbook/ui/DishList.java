package org.grakovne.cookbook.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.grakovne.cookbook.R;
import org.grakovne.cookbook.core.entity.Dish;
import org.grakovne.cookbook.core.services.DishService;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnTextChanged;

public class DishList extends AppCompatActivity {

    public static final String SELECTED_DISH = "SelectedDish";
    public static final String NEW_DISH = "New Dish";

    @InjectView(R.id.dishes_list_view)
    ListView dishesListView;

    @InjectView(R.id.find_dish_edit)
    EditText findDish;

    List<Dish> dishesToShow = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_list);
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Книга рецептов");

        showDishesInListView();

        if (DishService.getDishCount() == 0){
            Toast.makeText(getApplicationContext(), R.string.no_dishes, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dish_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_new_dish) {
            Intent intent = new Intent(this,  AddDishActivity.class);
            intent.putExtra(NEW_DISH, true);
            startActivity(intent);

            return true;
        }

        if (id == R.id.clear_dishes){
            DishService.deleteAllDishes();
            showDishesInListView();
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDishesInListView() {
        dishesToShow = DishService.getAllDishes();

        showSelectedDishes(dishesToShow);

        dishesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Dish selectedDish = dishesToShow.get(position);

                Intent intent = new Intent(getApplicationContext(), DishDetails.class);
                intent.putExtra(SELECTED_DISH, selectedDish);
                startActivity(intent);
            }
        });
    }

    private void showSelectedDishes(List<Dish> dishesToShow) {
        List<String> dishNameList = convertDishesToListView(dishesToShow);
        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dishNameList);
        dishesListView.setAdapter(adapter);

    }

    private List<String> convertDishesToListView(List<Dish> dishes){
        List<String> dishNames = new ArrayList<>();
        for (Dish dish: dishes){
            dishNames.add(dish.getDishName());
        }

        return dishNames;
    }

    @OnTextChanged(R.id.find_dish_edit)
    public void findDishes(){
        String filter = findDish.getText().toString();

        if (filter.isEmpty()){
            dishesToShow = DishService.getAllDishes();
        }
        else {
            dishesToShow = DishService.getFiltredDishes(filter.toLowerCase());
        }

        showSelectedDishes(dishesToShow);
    }

}
