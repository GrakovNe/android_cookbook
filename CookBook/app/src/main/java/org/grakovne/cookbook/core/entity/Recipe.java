package org.grakovne.cookbook.core.entity;


import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Recipe extends SugarRecord implements Serializable {
    private Long id;
    private Long dishId;

    @Ignore
    private List<RecipeStep> steps;

    public Recipe() {

    }

    public Recipe(Long dishId) {
        setDishId(dishId);
        steps = new ArrayList<RecipeStep>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Illegal ID");
        }
        this.id = id;
    }

    public void addRecipeStep(RecipeStep step) {
        steps.add(step);
    }

    public void deleteRecipeStep(Long recipeID) {
        for (int i = 0; i < steps.size(); i++) {
            if (steps.get(i).getId() == recipeID) {
                steps.remove(i);
                break;
            }
        }
    }

    public Long getDishId() {
        return dishId;
    }

    public void setDishId(Long dishId) {
        if (dishId == null) {
            throw new IllegalArgumentException("Illegal ID");
        }
        this.dishId = dishId;
    }

    public List<RecipeStep> getSteps() {
        return steps;
    }

    public void setSteps(List<RecipeStep> steps) {
        if (steps == null) {
            throw new IllegalArgumentException("Illegal Steps list");
        }
        this.steps = steps;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", dishId=" + dishId +
                ", steps=" + steps +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recipe recipe = (Recipe) o;

        if (id != null ? !id.equals(recipe.id) : recipe.id != null) return false;
        if (dishId != null ? !dishId.equals(recipe.dishId) : recipe.dishId != null) return false;
        return !(steps != null ? !steps.equals(recipe.steps) : recipe.steps != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (dishId != null ? dishId.hashCode() : 0);
        result = 31 * result + (steps != null ? steps.hashCode() : 0);
        return result;
    }
}
