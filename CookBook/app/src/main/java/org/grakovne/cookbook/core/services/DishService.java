package org.grakovne.cookbook.core.services;


import org.grakovne.cookbook.core.dao.DishDAO;
import org.grakovne.cookbook.core.entity.Dish;

import java.util.List;
import java.util.NoSuchElementException;

public class DishService {
    public static Long createNewDish(String dishName) {
        checkDishName(dishName);
        Dish dish = new Dish(dishName);

        DishDAO.addDish(dish);

        Long recipeId = RecipeService.createRecipe(dish.getId());
        RecipeStepService.createRecipeStep(recipeId, "У блюда еще нет рецепта!");

        return dish.getId();
    }

    public static int getDishCount(){
        return DishDAO.getAll().size();
    }

    public static Dish getDishByID(Long id){
        Dish dish = DishDAO.getDish(id);
        checkFound(dish);

        return dish;
    }

    public static Long changeDishName(Long id, String newDishName){
        Dish dish = getDishByID(id);
        dish.setDishName(newDishName);
        dish.save();

        return dish.getId();
    }

    public static List<Dish> getAllDishes(){
        return DishDAO.getAll();
    }

    public static List<Dish> getFiltredDishes(String ingredientName){
        return DishDAO.getDishesWithIngredient(ingredientName);
    }

    public static void deleteDish(Long id){
        DishDAO.deleteDish(id);
    }

    public static void deleteAllDishes(){
        DishDAO.deleteAll();
    }

    private static void checkDishName(String dishName) {
        if (dishName == null || dishName.isEmpty()) {
            throw new IllegalArgumentException("Invalid dish name");
        }
    }

    private static void checkFound(Dish dish){
        if (dish == null){
            throw new NoSuchElementException("Dish not found");
        }
    }

}
