package org.grakovne.cookbook.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.grakovne.cookbook.R;
import org.grakovne.cookbook.core.entity.Dish;
import org.grakovne.cookbook.core.entity.Recipe;
import org.grakovne.cookbook.core.entity.RecipeStep;
import org.grakovne.cookbook.core.services.DishService;
import org.grakovne.cookbook.core.services.RecipeService;
import org.grakovne.cookbook.core.services.RecipeStepService;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class RecipeStepDetails extends AppCompatActivity {

    public static final String SELECTED_RECIPE = "SelectedRecipe";
    @InjectView(R.id.what_we_cook_text)
    TextView whatWeCookText;
    @InjectView(R.id.step_number_text)
    TextView stepNumberText;
    @InjectView(R.id.instruction_text)
    TextView instructionText;
    @InjectView(R.id.back_button)
    Button backButton;
    @InjectView(R.id.forward_button)
    Button forwardButton;

    private int currentStep = 1;
    private int maximalStep;
    private List<RecipeStep> stepList;
    private Dish dish = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_step_details);
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Книга рецептов");

        Intent intent = getIntent();

        if (intent.hasExtra(SELECTED_RECIPE)) {
            Recipe recipe = (Recipe) intent.getSerializableExtra(SELECTED_RECIPE);
            dish = DishService.getDishByID(recipe.getDishId());

            stepList = recipe.getSteps();
            maximalStep = stepList.size();

            if (intent.hasExtra("CurrentStep")){
                currentStep = intent.getIntExtra("CurrentStep", 1);
            }

            setInstructionText();
            setCurrentStep();


            whatWeCookText.setText(dish.getDishName());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_recipe_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_new_step) {
            Intent intent = new Intent(this, AddRecipeStepActivity.class);
            intent.putExtra("AddStep", true);
            intent.putExtra("Dish", dish);
            startActivity(intent);

            return true;
        }

        if (id == R.id.edit_step){
            Intent intent = new Intent(this, AddRecipeStepActivity.class);
            intent.putExtra("EditStep", true);
            intent.putExtra("Dish", dish);

            intent.putExtra("RecipeStep", stepList.get(currentStep - 1));
            startActivity(intent);

            return true;
        }

        if (id == R.id.delete_step){
            RecipeStepService.removeRecipeStep(RecipeService.getRecipeByDishId(dish.getId()).getId(), currentStep--);
            stepList = RecipeService.getRecipeByDishId(dish.getId()).getSteps();

            if (stepList.size() == 0){
                RecipeStepService.createRecipeStep(RecipeService.getRecipeByDishId(dish.getId()).getId(),  "У блюда еще нет рецепта!");
            }

            Recipe recipe = RecipeService.getRecipeByDishId(dish.getId());
            stepList = recipe.getSteps();
            maximalStep = stepList.size();

            setInstructionText();
            setCurrentStep();


            /*
            Intent intent = new Intent(this, RecipeStepDetails.class);
            intent.putExtra(SELECTED_RECIPE, recipe);
            intent.putExtra("RecipeStep", currentStep);

            startActivity(intent);
            */
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.back_button)
    public void onBackButtonClick(View view) {
        if (currentStep > 1) {
            currentStep--;
            setInstructionText();
            setCurrentStep();
        }
    }

    @OnClick(R.id.forward_button)
    public void onForwardButtonClick(View view) {
        if (currentStep < maximalStep) {
            currentStep++;
            setInstructionText();
            setCurrentStep();
        }

    }

    private void setCurrentStep() {
        StringBuilder builder = new StringBuilder();

        builder
                .append("Текущий шаг: ")
                .append(currentStep)
                .append(" / ")
                .append(maximalStep);
        stepNumberText.setText(builder.toString());
    }

    private void setInstructionText() {
        if (currentStep > 0) {
            instructionText.setText(stepList.get(currentStep - 1).getInstruction());
        } else {
            currentStep = 1;
            instructionText.setText(stepList.get(0).getInstruction());
        }
    }
}
