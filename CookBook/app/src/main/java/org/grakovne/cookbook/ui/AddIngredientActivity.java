package org.grakovne.cookbook.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.grakovne.cookbook.R;
import org.grakovne.cookbook.core.entity.Dish;
import org.grakovne.cookbook.core.entity.Ingredient;
import org.grakovne.cookbook.core.services.DishService;
import org.grakovne.cookbook.core.services.IngredientService;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class AddIngredientActivity extends AppCompatActivity {

    public static final String EDIT_INGREDIENT = "Edit ingredient";
    public static final String INGREDIENT = "Ingredient";

    @InjectView(R.id.add_ingredient_hint)
    TextView addIngredientView;

    @InjectView(R.id.add_ingredient_name)
    EditText addIngredientName;

    @InjectView(R.id.add_ingredient_button)
    Button addIngredientButton;

    public static final String ADD_INGREDIENT = "Add ingredient";
    public static final String DISH = "Dish";
    Dish dish = null;
    Ingredient ingredient = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ingredient);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.inject(this);
        setTitle("Книга рецептов");


        Intent intent = getIntent();
        dish = (Dish) intent.getSerializableExtra(DISH);

        if (intent.hasExtra(ADD_INGREDIENT)){
            addIngredientView.setText(R.string.add_ingredient_hint);
            ingredient = null;
        }

        if (intent.hasExtra(EDIT_INGREDIENT)){
            addIngredientView.setText(R.string.edit_ingredient_hint);
            ingredient = (Ingredient) intent.getSerializableExtra(INGREDIENT);
            addIngredientName.setText(ingredient.getIngredientName());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (ingredient != null) {
            getMenuInflater().inflate(R.menu.menu_ingredient_details, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.delete_ingredient) {
            IngredientService.deleteIngredient(ingredient.getId());
            returnToDishDetails();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.add_ingredient_button)
    public void onAddIngredientButtonClick() {
            String newIngredientName = addIngredientName.getText().toString();

            if (!checkIngredientName(newIngredientName)) {
                Toast.makeText(getApplicationContext(), R.string.empty_ingredient_name, Toast.LENGTH_SHORT).show();
                return;
            }

            if (ingredient == null) {
                Long id = IngredientService.createIngredient(dish.getId(), newIngredientName);
            } else {
                IngredientService.changeIngredientName(ingredient.getId(), newIngredientName);
            }

            returnToDishDetails();
    }


    private boolean checkIngredientName(String ingredientName) {
        if (ingredientName == null || ingredientName.isEmpty()) {
            return false;
        }

        return true;
    }

    private void returnToDishDetails(){
        Intent intent = new Intent(this, DishDetails.class);
        dish = DishService.getDishByID(dish.getId());
        intent.putExtra("SelectedDish", dish);
        startActivity(intent);}
}

