package org.grakovne.cookbook.core.services;

import org.grakovne.cookbook.core.dao.RecipeStepDAO;
import org.grakovne.cookbook.core.entity.Recipe;
import org.grakovne.cookbook.core.entity.RecipeStep;

import java.util.List;

public class RecipeStepService {
    public static Long createRecipeStep(Long recipeId, String instruction){
        Recipe recipe = RecipeService.getRecipeById(recipeId);

        RecipeStep recipeStep = new RecipeStep(recipeId, recipe.getSteps().size(), instruction);
        RecipeStepDAO.addRecipeStep(recipeStep);

        return recipeStep.getId();
    }

    public static RecipeStep getRecipeStep(Long recipeStepId){
        return RecipeStepDAO.getRecipeStep(recipeStepId);
    }

    public static RecipeStep getRecipeStep(Long recipeId, int number){
        Recipe recipe = RecipeService.getRecipeById(recipeId);
        List<RecipeStep> recipeSteps = recipe.getSteps();

        return recipeSteps.get(number - 1);
    }

    public static void changeRecipeStep(Long recipeId, int number, String newInstruction){
        Recipe recipe = RecipeService.getRecipeById(recipeId);
        RecipeStep recipeStep = recipe.getSteps().get(number - 1);
        recipeStep.setInstruction(newInstruction);
        recipeStep.save();
    }

    public static void removeRecipeStep(Long recipeId, int number){
        RecipeStep recipeStep = getRecipeStep(recipeId, number);

        List<RecipeStep> steps = RecipeService.getRecipeById(recipeId).getSteps();

        for (int i = number + 1; i < steps.size(); i++){
            RecipeStep step =  steps.get(i);
            int stepNumber = step.getStepNumber();
            step.setStepNumber(stepNumber - 1);
            step.save();
        }

        RecipeStepDAO.deleteRecipeStep(recipeStep.getId());
    }
}
