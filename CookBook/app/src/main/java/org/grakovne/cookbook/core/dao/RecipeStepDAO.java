package org.grakovne.cookbook.core.dao;

import org.grakovne.cookbook.core.entity.Recipe;
import org.grakovne.cookbook.core.entity.RecipeStep;

import java.util.Iterator;
import java.util.List;

public class RecipeStepDAO {
    public static Long addRecipeStep(RecipeStep recipeStep) {
        return recipeStep.save();
    }

    public static void deleteRecipeStep(Long id) {
        RecipeStep recipeStep = RecipeStep.findById(RecipeStep.class, id);

        if (recipeStep == null) {
            throw new RuntimeException("Cannot delete non-existent recipe step");
        }

        recipeStep.delete();
    }

    public static RecipeStep getRecipeStep(Long id){
        return RecipeStep.findById(RecipeStep.class, id);
    }

    public static void deleteAll(){
        List<RecipeStep> steps = RecipeStep.listAll(RecipeStep.class);

        Iterator<RecipeStep> iterator = steps.iterator();

        while (iterator.hasNext()){
            deleteRecipeStep(iterator.next().getId());
        }
    }


}
