package org.grakovne.cookbook.core.dao;

import org.grakovne.cookbook.core.entity.Recipe;
import org.grakovne.cookbook.core.entity.RecipeStep;

import java.util.Iterator;
import java.util.List;

public class RecipeDAO {
    public static Long addRecipe(Recipe recipe){
        return recipe.save();
    }

    public static void saveRecipeSteps(Recipe recipe){
        List<RecipeStep> recipeSteps = recipe.getSteps();

        Iterator<RecipeStep> iterator = recipeSteps.iterator();

        while (iterator.hasNext()){
            RecipeStepDAO.addRecipeStep(iterator.next());
        }
    }

    public static Recipe getRecipe(Long id){
        Recipe recipe = Recipe.findById(Recipe.class, id);
        recipe = getSteps(recipe);
        return recipe;
    }

    public static Recipe getRecipeByDishID(Long dishId){
        List<Recipe> recipes = Recipe.find(Recipe.class, "dish_id = ?", String.valueOf(dishId));
        return getRecipe(recipes.get(0).getId());
    }

    public static void deleteRecipe(Long id){
        Recipe recipe = Recipe.findById(Recipe.class, id);

        if (recipe == null){
            throw new RuntimeException("Cannot delete non-existent recipe");
        }

        List<RecipeStep> recipeSteps = recipe.getSteps();

        if (recipeSteps == null){
            return;
        }

        Iterator<RecipeStep> iterator = recipeSteps.iterator();

        while (iterator.hasNext()){
            RecipeStepDAO.deleteRecipeStep(iterator.next().getId());
        }

        recipe.delete();
    }

    private static Recipe getSteps(Recipe recipe){
        List<RecipeStep> recipeSteps = RecipeStep.find(RecipeStep.class, "recipe_id = ?", String.valueOf(recipe.getId()));
        recipe.setSteps(recipeSteps);
        return recipe;
    }
}
