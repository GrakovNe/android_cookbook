package org.grakovne.cookbook.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.grakovne.cookbook.R;
import org.grakovne.cookbook.core.entity.Dish;
import org.grakovne.cookbook.core.entity.Recipe;
import org.grakovne.cookbook.core.entity.RecipeStep;
import org.grakovne.cookbook.core.services.RecipeService;
import org.grakovne.cookbook.core.services.RecipeStepService;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class AddRecipeStepActivity extends AppCompatActivity {

    Dish dish = null;
    Recipe recipe = null;
    RecipeStep recipeStep = null;

    @InjectView(R.id.add_step_hint)
    TextView addStepHint;

    @InjectView(R.id.instruction_text)
    EditText instructionText;

    @InjectView(R.id.save_instruction_button)
    Button saveInstruction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe_step);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.inject(this);
        setTitle("Книга рецептов");

        Intent intent = getIntent();
        dish = (Dish) intent.getSerializableExtra("Dish");
        recipe = RecipeService.getRecipeByDishId(dish.getId());

        if (intent.hasExtra("AddStep")){
            addStepHint.setText(R.string.add_new_step_text);
            recipeStep = null;
        }

        if (intent.hasExtra("EditStep")) {
            addStepHint.setText(R.string.edit_step_text);
            recipeStep = (RecipeStep) intent.getSerializableExtra("RecipeStep");
            instructionText.setText(recipeStep.getInstruction());
        }

    }

    @OnClick(R.id.save_instruction_button)
    public void onSaveInstructionClick(){
        String newInstruction = instructionText.getText().toString();

        if (!checkInstruction(newInstruction)) {
            Toast.makeText(this, R.string.empty_instruction, Toast.LENGTH_SHORT).show();
            return;
        }

        if (recipeStep == null){
            Long id = RecipeStepService.createRecipeStep(recipe.getId(), newInstruction);
            recipeStep = RecipeStepService.getRecipeStep(id);
        } else {
            RecipeStepService.changeRecipeStep(recipe.getId(), recipeStep.getStepNumber() + 1, newInstruction);
        }

        returnToStepDetails();
    }

    private boolean checkInstruction(String instruction) {
        if (instruction == null || instruction.isEmpty()) {
            return false;
        }

        return true;
    }

    private void returnToStepDetails(){
        recipe = RecipeService.getRecipeById(recipe.getId());

        Intent intent = new Intent(this, RecipeStepDetails.class);
        intent.putExtra("SelectedRecipe", recipe);
        intent.putExtra("CurrentStep", recipeStep.getStepNumber() + 1);

        startActivity(intent);
    }
}
