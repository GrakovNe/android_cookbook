package org.grakovne.cookbook.core.entity;

import com.orm.SugarRecord;

import java.io.Serializable;

public class Ingredient  extends SugarRecord  implements Serializable {
    private Long id;
    private Long dishId;
    private String ingredientName;

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        if (ingredientName == null || ingredientName.isEmpty()){
            throw new IllegalArgumentException("Illegal ingredient");
        }
        this.ingredientName = ingredientName.toLowerCase();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        if (id == null){
            throw new IllegalArgumentException("Illegal Identity");
        }
        this.id = id;
    }

    public Long getDishId() {
        return dishId;
    }

    public void setDishId(Long dishId) {
        this.dishId = dishId;
    }

    public Ingredient(Long dishId, String ingredientName){
        setDishId(dishId);
        setIngredientName(ingredientName);
    }

    public Ingredient(){

    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "id=" + id +
                ", dishId=" + dishId +
                ", ingredientName='" + ingredientName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ingredient that = (Ingredient) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (dishId != null ? !dishId.equals(that.dishId) : that.dishId != null) return false;
        return !(ingredientName != null ? !ingredientName.equals(that.ingredientName) : that.ingredientName != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (dishId != null ? dishId.hashCode() : 0);
        result = 31 * result + (ingredientName != null ? ingredientName.hashCode() : 0);
        return result;
    }
}
