package org.grakovne.cookbook.core.services;

import org.grakovne.cookbook.core.dao.IngredientDAO;
import org.grakovne.cookbook.core.entity.Dish;
import org.grakovne.cookbook.core.entity.Ingredient;

import java.util.NoSuchElementException;

public class IngredientService {

    public static Long createIngredient(Long dishId, String ingredientName){
        Dish dish = DishService.getDishByID(dishId);

        Ingredient ingredient = new Ingredient(dishId, ingredientName);
        ingredient.save();

        return ingredient.getId();
    }

    public static Ingredient getIngredient(Long id){
        Ingredient ingredient = IngredientDAO.getIngredient(id);
        checkIngredient(ingredient);

        return ingredient;
    }

    public static void deleteIngredient(Long id){
        Ingredient ingredient = getIngredient(id);
        ingredient.delete();
    }

    public static Long changeIngredientName(Long ingredientId, String newIngredientName){
        Ingredient ingredient = getIngredient(ingredientId);
        ingredient.setIngredientName(newIngredientName);
        ingredient.save();

        return ingredient.getId();
    }

    private static void checkIngredient(Ingredient ingredient) {
        if (ingredient == null) {
            throw new NoSuchElementException("Ingredient not found");
        }
    }

}
