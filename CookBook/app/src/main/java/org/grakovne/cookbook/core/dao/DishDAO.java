package org.grakovne.cookbook.core.dao;

import android.util.Log;

import org.grakovne.cookbook.core.entity.Dish;
import org.grakovne.cookbook.core.entity.Ingredient;
import org.grakovne.cookbook.core.entity.Recipe;

import java.io.Flushable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DishDAO {

    public static Long addDish(Dish dish) {
        dish.save();
        return dish.getId();
    }

    public static List<Dish> getAll() {
        List<Dish> dishes = Dish.listAll(Dish.class);
        List<Dish> resultDish = new ArrayList<>();

        Iterator<Dish> iterator = dishes.iterator();

        while (iterator.hasNext()) {
            Dish dish = getIngredients(iterator.next());
            resultDish.add(dish);
        }

        return resultDish;
    }

    public static void deleteDishesIngredients(Dish dish) {
        List<Ingredient> ingredients = dish.getIngredients();

        Iterator<Ingredient> iterator = ingredients.iterator();

        while (iterator.hasNext()) {
            IngredientDAO.deleteIngredient(iterator.next().getId());
        }
    }

    public static void saveDishIngredients(Dish dish) {
        for (Ingredient ingredient : dish.getIngredients()) {
            IngredientDAO.addIngredient(ingredient);
        }
    }

    public static void deleteDish(Long id) {
        Dish dish = Dish.findById(Dish.class, id);
        Recipe recipe = RecipeDAO.getRecipeByDishID(dish.getId());

        dish = getIngredients(dish);


        if (dish == null) {
            throw new RuntimeException("Cannot delete non-existent dish");
        }

        if (dish.getIngredients().size() != 0) {
            for (Ingredient ingredient : dish.getIngredients()) {
                IngredientDAO.deleteIngredient(ingredient.getId());
            }
        }

        RecipeDAO.deleteRecipe(recipe.getId());


        dish.delete();
    }

    public static void deleteAll(){
        List<Dish> dishes = Dish.listAll(Dish.class);
        Iterator<Dish> iterator = dishes.iterator();

        while (iterator.hasNext()){
            Dish dish = iterator.next();
            if (dish != null) {
                deleteDish(dish.getId());
            }
        }
    }

    public static Dish getDish(Long id) {
        Dish dish = Dish.findById(Dish.class, id);
        dish = getIngredients(dish);
        return dish;
    }

    private static Dish getIngredients(Dish dish) {
        List<Ingredient> ingredients = Ingredient.find(Ingredient.class, "dish_id = ?", String.valueOf(dish.getId()));
        dish.setIngredients(ingredients);
        return dish;
    }

    public static List<Dish> getDishesWithIngredient(String ingredientName) {
        List<Ingredient> ingredients = IngredientDAO.getIngredient(ingredientName);

        Log.d("CookBook", ingredients.toString());

        List<Dish> dishes = new ArrayList<>();

        if (ingredients.size() == 0) {
            return dishes;
        }

        for (Ingredient ingredient : ingredients) {
            if (ingredient.getDishId() == null){
                continue;
            }
            dishes.add(DishDAO.getDish(ingredient.getDishId()));
        }

        return dishes;
    }

}
